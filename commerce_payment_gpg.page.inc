<?php



function commerce_payment_gpg_process_ipn($payment_method = NULL) {

  $order = NULL;
  $skip_process = FALSE;
  
  
  
  // Attempt to load payment method from alternative means.
  if (!isset($payment_method)) {
    if (!empty($wppr['Signature']) && $prior_wppr = commerce_payment_gpg_txn_load($wppr['Signature'], 'signature')) {
      if (!empty($prior_wppr['transaction_id'])) {
        $transaction = commerce_payment_transaction_load($prior_wppr['transaction_id']);
        if (empty($wppr['PAYID'])) {
          $wppr['PAYID'] = $transaction->order_id;
        }
        elseif ($wppr['PAYID'] != $transaction->order_id) {
		
          // Why do we have two different order IDs? Lets bail.
          exit('');
        }
        $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
      }
    }
    
	
    // @todo Is this safe? Can we end up with the wrong payment method from the
    // Order object?
    if (!$payment_method && !empty($wppr['PAYID'])) {
      if ($order = commerce_order_load($wppr['PAYID'])) {
        if (!empty($order->data['payment_method'])) {
          $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
		  
        }
      }
      else {
        print '';
        return;
      }
    }
  }
  
  
  /*if ($payment_method) {
    if ($payment_method['method_id'] != 'commerce_payment_gpg') {
      print '';
      return;
    }
  }
  else {
    // Bail since we have no useful data to work with.
    print '';
    return;
  }*/
  
    // WorldPay Payment Response.
    $wppr = $_POST;
	
  if (empty($wppr['Signature'])) {
      if ($payment_method['settings']['debug'] == 'Log') {
        watchdog(
          'commerce_payment_gpg',
          'Request with no signature was sent from <em>@ip</em> with request method <b>@method</b>.',
          array(
            '@ip' => ip_address(),
            '@method' => !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '',
          ),
          WATCHDOG_NOTICE);
      }
      return;
    }

  if (empty($wppr['PAYID'])) {
      drupal_add_http_header('Status', '404 Not Found');
      if ($payment_method['settings']['debug']) {
        watchdog(
          'commerce_payment_gpg',
          'Request with no PAYID was sent from <em>@ip</em> with request method <b>@method</b>.',
          array(
            '@ip' => ip_address(),
            '@method' => !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '',
          ),
          WATCHDOG_NOTICE);
      }
      return;
    }
	
	
  if (!empty($wppr['TotalAmount']) && ($wppr['TotalAmount'] >= 1000) ) {
		$wppr['TotalAmount'] = $wppr['TotalAmount'] / 1000 ;
    } else {
		if ($payment_method['settings']['debug']) {
        watchdog(
          'commerce_payment_gpg',
          'Request with bad amount from <em>@ip</em> with request method <b>@method</b>.',
          array(
            '@ip' => ip_address(),
            '@method' => !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '',
          ),
          WATCHDOG_NOTICE);
      }
      return;
	}
  
  if (!$order && !$order = commerce_order_load($wppr['PAYID'])) {
    drupal_add_http_header('Status', '404 Not Found');
    return;
  }

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $authenticated = _commerce_payment_gpg_response_authenticate(
      $order_wrapper,
      $wppr,
      $payment_method,
      $prior_wppr);

    if (!$authenticated) {
	  watchdog('commerce_payment_gpg', "not authenticated transaction");
      return;
    }

  switch ($wppr['TransStatus']) {
    case '00':
        // Create or re-establish a Commerce Transaction.
        // This will add the 'transaction_id' to $wppr.
        commerce_payment_gpg_transaction_process($order, $payment_method, $wppr, $prior_wppr);
        $tx_data = commerce_payment_gpg_convert_wppr_to_record($wppr);
        $tx_data['transaction_id'] = $wppr['transaction_id'];
        $tx_data['order_id'] = $order->order_id;
        if ($prior_wppr) {
          $tx_data = array_replace($prior_wppr, $tx_data);
        }
        commerce_payment_gpg_txn_save($tx_data);

      if ($payment_method['settings']['debug'] == 'Log') {
        watchdog(
          'commerce_payment_gpg', 'Creating success HTML for transaction @wp_txn_id.',
          array(
            '@signature' => $wppr['Signature'],
          ),
          WATCHDOG_NOTICE);
      }

      break;

    case '06':
      $theme_data['settings']['context'] = 'result_cancel';
      if ($prior_wppr) {
        // Don't know if GPG ever changes transStatus after it's been set
        // the first time, but if it does we update our transaction info.
        $tx_data = commerce_payment_gpg_convert_wppr_to_record($wppr);
        $tx_data = array_replace($prior_wppr, $tx_data);
        commerce_payment_gpg_txn_save($tx_data);
      }

      // Not totally sure if this is the right thing to do here?
      commerce_payment_redirect_pane_previous_page($order);

      if (!empty($payment_method) and $payment_method['settings']['debug'] == 'Log') {
        watchdog(
          'commerce_payment_gpg', 'Creating cancel HTML for transaction @wp_txn_id.',
          array(
            '@signature' => $wppr['Signature'],
          ),
          WATCHDOG_NOTICE);
      }

      break;
  }

//fin callback
}

function _commerce_payment_gpg_response_authenticate($order_wrapper, &$wppr, &$payment_method = NULL, &$wp_transaction = NULL) {
  $failed_authenticaion = FALSE;
  $message = '';
  $settings = $payment_method['settings'];
  $sha1 = sha1($wppr['TransStatus'].$wppr['PAYID'].$settings['payment_security']['password']);

  if (!empty($settings['payment_security']['password'])) {
    if ($sha1 != $wppr['Signature']) {
      $failed_authenticaion = TRUE;
      $message = 'GPG passwords do not match. Make sure you have the same password set in the Commerce GPG settings page as set in your GPG installtion.';
    }
  }

  // @todo Reverse DNS lookup on IP address.
  if ($failed_authenticaion) {
    drupal_add_http_header('Status', '403 Forbidden');
    $ip = ip_address();
    watchdog('commerce_payment_gpg', 'Access denied! ' . $message . ' Clients details: <em>@ip</em> with request method <b>@method</b>. Refered by: <b>@referer</b>.',
      array(
        '@ip' => !empty($ip) ? $ip : '0.0.0.0',
        '@method' => !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '',
        '@referer' => !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'UNKNOWN',
      ), WATCHDOG_WARNING);
    return FALSE;
  }

  return TRUE;
}


function commerce_payment_gpg_transaction_process($order, $payment_method, &$wppr, &$prior_wppr = NULL) {
  // Exit when we don't get a payment status we recognize.
  if (!in_array($wppr['TransStatus'], array('00', '05', '06'))) {
    commerce_payment_redirect_pane_previous_page($order);
    return FALSE;
  }

  // If this is a prior authorization capture GPG Payment Response for
  // which we've already created a transaction...
  if ($prior_wppr) {
    // Load the prior IPN's transaction and update that with the capture values.
    $transaction = commerce_payment_transaction_load($prior_wppr['transaction_id']);
  }
  else {
    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new('commerce_payment_gpg', $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];
  }

  $transaction->remote_id = $wppr['Signature'];/** warning **/
  $transaction->amount = commerce_currency_decimal_to_amount($wppr['TotalAmount'], $wppr['Currency']);
  $transaction->currency_code = $wppr['Currency'];
  $transaction->payload[REQUEST_TIME] = $wppr;
  // Remove sensitive data.
  unset($transaction->payload[REQUEST_TIME]['callbackPW']);

  // Set the transaction's statuses based on the IPN's payment_status.
  $transaction->remote_status = $wppr['TransStatus'];

  // @todo - Figure out how best to  set status based on SecureCode
  //   authentication.
  /** Warning **/
  if (isset($wppr['authentication'])) {
    switch ($wppr['authentication']) {
      case 'ARespH.card.authentication.0':
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        $transaction->message = t("Cardholder authenticated by SecureCode.");
        break;

      case 'ARespH.card.authentication.1':
        $transaction->message = t('Cardholder/Issuing Bank not enrolled for authentication.');
        break;

      case 'ARespH.card.authentication.6':
        $transaction->message = t('Cardholder authentication not available');
        break;

      case 'ARespH.card.authentication.7':
        $transaction->message = t('Cardholder did not complete authentication.');
        break;
    }
  }

  // They don't give us very detailed transaction information do they?
  switch ($wppr['TransStatus']) {
    case '00':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t("GPG accepted the user's transaction.");
      break;

    // I don't think we should ever see this status at this point but incase
    // we do...
    case '05':
      // @todo - Is this a suitable status?
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Payment Refused');
      break;

    // I don't think we should ever see this status at this point but incase
    // we do...
    case '06':
      // @todo - Is this a suitable status?
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t('Payment Canceled');
      break;

  }

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);
  $wppr['transaction_id'] = $transaction->transaction_id;

  commerce_payment_redirect_pane_next_page($order);
  watchdog('commerce_payment_gpg', 'Payment Response processed for Order @order_number with ID @txn_id.', array('@txn_id' => $wppr['Signature'], '@order_number' => $order->order_number), WATCHDOG_INFO);
}


